//
//  PBEditViewController.m
//  PearlBox
//
//  Created by Kevin Cao on 12-12-19.
//  Copyright (c) 2012年 sumi-sumi.com. All rights reserved.
//

#import "PBEditViewController.h"

@interface PBEditViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation PBEditViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.textView.text = self.pearl.content;
}

- (void)viewDidUnload
{
	[self setTextView:nil];
	[super viewDidUnload];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	[self.textView becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelAction:(id)sender
{
	[self.delegate editViewController:self didFinishSave:NO];
}

- (IBAction)saveAction:(id)sender
{
	if (self.pearl.isDeleted) {
		// if deleted by iCloud, duplicate a new one
		self.pearl = [Pearl pearlWithPearl:self.pearl];
	}
	
	if (![self.textView.text isEqualToString:self.pearl.content]) {
		self.pearl.content = self.textView.text;
		self.pearl.updatedAt = [NSDate date];
	}
	
	[self.delegate editViewController:self didFinishSave:YES];
}


@end
