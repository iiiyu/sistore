//
//  SIDemoStore.m
//  SIStoreDemo
//
//  Created by Xiao ChenYu on 13-3-6.
//  Copyright (c) 2013年 Sumi Interactive. All rights reserved.
//

#import "SIDemoStore.h"
#import "Pearl.h"

@implementation SIDemoStore

+ (BOOL)seedStore:(NSPersistentStore *)store withPersistentStoreAtURL:(NSURL *)seedStoreURL error:(NSError *__autoreleasing *)error
{
    BOOL success = YES;
    NSError *localError = nil;
    
    NSManagedObjectModel *model = [NSManagedObjectModel mergedModelFromBundles:nil];
    NSPersistentStoreCoordinator *seedPSC = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
    NSDictionary *seedStoreOptions = @{ NSReadOnlyPersistentStoreOption : @YES };
    NSPersistentStore *seedStore = [seedPSC addPersistentStoreWithType:NSSQLiteStoreType
                                                         configuration:nil
                                                                   URL:seedStoreURL
                                                               options:seedStoreOptions
                                                                 error:&localError];
    if (seedStore) {
        NSManagedObjectContext *seedMOC = [[NSManagedObjectContext alloc] init];
		seedMOC.persistentStoreCoordinator = seedPSC;
        
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Pearl"];
        NSUInteger batchSize = 5000;
		fetchRequest.fetchBatchSize = batchSize;
		
        NSArray *results = [seedMOC executeFetchRequest:fetchRequest error:&localError];
		
        NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
		moc.persistentStoreCoordinator = [NSPersistentStoreCoordinator MR_defaultStoreCoordinator];
        
        NSUInteger i = 1;
        for (Pearl *pearl in results) {
			
			Pearl *newPearl = [Pearl pearlWithPearl:pearl inContext:moc];
			[moc assignObject:newPearl toPersistentStore:store];
			
            if (0 == (i % batchSize)) {
                success = [moc save:&localError];
                if (success) {
                    /*
                     Reset the managed object context to free the memory for the inserted objects
                     The faulting array used for the fetch request will automatically free objects
                     with each batch, but inserted objects remain in the managed object context for
                     the lifecycle of the context
                     */
                    [moc reset];
                } else {
                    NSLog(@"Error saving during seed: %@", localError);
                    break;
                }
            }
            
            i++;
        }
        
        //one last save
        if ([moc hasChanges]) {
            success = [moc save:&localError];
            [moc reset];
        }
    } else {
        success = NO;
        NSLog(@"Error adding seed store: %@", localError);
    }
    
    if (NO == success) {
        if (localError  && (error != NULL)) {
            *error = localError;
        }
    }
	
    return success;
}

+ (void)deDupe
{
    @autoreleasepool {
		NSString *entityName = @"Pearl";
		
        NSError *error = nil;
		
        NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] init];
		moc.persistentStoreCoordinator = [NSPersistentStoreCoordinator MR_defaultStoreCoordinator];
		
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entityName];
        fetchRequest.includesPendingChanges = NO; //distinct has to go down to the db, not implemented for in memory filtering
		fetchRequest.fetchBatchSize = 1000; //protect the memory
        
        NSExpression *expression = [NSExpression expressionWithFormat:@"count:(createdAt)"];
        NSExpressionDescription *expressionDescription = [[NSExpressionDescription alloc] init];
		expressionDescription.name = @"count";
		expressionDescription.expression = expression;
		expressionDescription.expressionResultType = NSInteger64AttributeType;
        
        NSAttributeDescription *attributeDescription = [[[NSManagedObjectModel MR_defaultManagedObjectModel] entitiesByName][entityName] propertiesByName][@"createdAt"];
		fetchRequest.propertiesToFetch = @[attributeDescription, expressionDescription];
		fetchRequest.propertiesToGroupBy = @[attributeDescription];
        
		fetchRequest.resultType = NSDictionaryResultType;
        
        NSArray *dictionaryResults = [moc executeFetchRequest:fetchRequest error:&error];
        NSMutableArray *keyWithDupes = [[NSMutableArray alloc] init];
        for (NSDictionary *dictionary in dictionaryResults) {
            NSNumber *count = dictionary[@"count"];
            if ([count integerValue] > 1) {
                [keyWithDupes addObject:dictionary[@"createdAt"]];
            }
        }
        
        NSLog(@"%@ with dupes: %@", @"createdAt", keyWithDupes);
        
        //fetch out all the duplicate records
        fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entityName];
		fetchRequest.includesPendingChanges = NO;
		fetchRequest.predicate = [NSPredicate predicateWithFormat:@"createdAt IN (%@)", keyWithDupes];
		fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"createdAt" ascending:YES]];
		NSUInteger batchSize = 500;
		fetchRequest.fetchBatchSize = batchSize; //can be set 100-10000 objects depending on individual object size and available device memory
        
        NSArray *dupes = [moc executeFetchRequest:fetchRequest error:&error];
        
        Pearl *prevResult = nil;
        NSUInteger i = 1;
        for (Pearl *result in dupes) {
            if (prevResult) {
                if ([result.createdAt isEqualToDate:prevResult.createdAt]) {
                    [moc deleteObject:result];
                } else {
                    prevResult = result;
                }
            } else {
                prevResult = result;
            }
            
            if (0 == (i % batchSize)) {
                //save the changes after each batch, this helps control memory pressure by turning previously examined objects back in to faults
                if ([moc save:&error]) {
                    NSLog(@"Saved successfully after uniquing");
                } else {
                    NSLog(@"Error saving unique results: %@", error);
                }
            }
            
            i++;
        }
        
        if ([moc save:&error]) {
            NSLog(@"Saved successfully after uniquing");
        } else {
            NSLog(@"Error saving unique results: %@", error);
        }
    }
}

@end
