//
//  PBEditViewController.h
//  PearlBox
//
//  Created by Kevin Cao on 12-12-19.
//  Copyright (c) 2012年 sumi-sumi.com. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "Pearl+PBAddition.h"
#import "Pearl.h"

@protocol PBEditViewControllerDelegate;

@interface PBEditViewController : UIViewController

@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) Pearl *pearl;
@property (nonatomic, weak) id<PBEditViewControllerDelegate> delegate;

@end

@protocol PBEditViewControllerDelegate <NSObject>

- (void)editViewController:(PBEditViewController *)viewController didFinishSave:(BOOL)save;

@end
