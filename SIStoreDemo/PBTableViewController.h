//
//  PBTableViewController.h
//  PearlBox
//
//  Created by Kevin Cao on 12-12-19.
//  Copyright (c) 2012年 sumi-sumi.com. All rights reserved.
//

#import "CoreDataTableViewController.h"

@interface PBTableViewController : CoreDataTableViewController

@end
