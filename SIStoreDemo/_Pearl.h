// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Pearl.h instead.

#import <CoreData/CoreData.h>


extern const struct PearlAttributes {
	__unsafe_unretained NSString *archived;
	__unsafe_unretained NSString *content;
	__unsafe_unretained NSString *createdAt;
	__unsafe_unretained NSString *position;
	__unsafe_unretained NSString *test;
	__unsafe_unretained NSString *updatedAt;
} PearlAttributes;

extern const struct PearlRelationships {
} PearlRelationships;

extern const struct PearlFetchedProperties {
} PearlFetchedProperties;









@interface PearlID : NSManagedObjectID {}
@end

@interface _Pearl : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (PearlID*)objectID;





@property (nonatomic, strong) NSNumber* archived;



@property BOOL archivedValue;
- (BOOL)archivedValue;
- (void)setArchivedValue:(BOOL)value_;

//- (BOOL)validateArchived:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* content;



//- (BOOL)validateContent:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* createdAt;



//- (BOOL)validateCreatedAt:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* position;



@property int64_t positionValue;
- (int64_t)positionValue;
- (void)setPositionValue:(int64_t)value_;

//- (BOOL)validatePosition:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* test;



@property BOOL testValue;
- (BOOL)testValue;
- (void)setTestValue:(BOOL)value_;

//- (BOOL)validateTest:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* updatedAt;



//- (BOOL)validateUpdatedAt:(id*)value_ error:(NSError**)error_;






@end

@interface _Pearl (CoreDataGeneratedAccessors)

@end

@interface _Pearl (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveArchived;
- (void)setPrimitiveArchived:(NSNumber*)value;

- (BOOL)primitiveArchivedValue;
- (void)setPrimitiveArchivedValue:(BOOL)value_;




- (NSString*)primitiveContent;
- (void)setPrimitiveContent:(NSString*)value;




- (NSDate*)primitiveCreatedAt;
- (void)setPrimitiveCreatedAt:(NSDate*)value;




- (NSNumber*)primitivePosition;
- (void)setPrimitivePosition:(NSNumber*)value;

- (int64_t)primitivePositionValue;
- (void)setPrimitivePositionValue:(int64_t)value_;




- (NSNumber*)primitiveTest;
- (void)setPrimitiveTest:(NSNumber*)value;

- (BOOL)primitiveTestValue;
- (void)setPrimitiveTestValue:(BOOL)value_;




- (NSDate*)primitiveUpdatedAt;
- (void)setPrimitiveUpdatedAt:(NSDate*)value;




@end
