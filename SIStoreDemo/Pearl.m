#import "Pearl.h"


@interface Pearl ()

// Private interface goes here.

@end


@implementation Pearl

// Custom logic goes here.

+ (Pearl *)pearlWithContent:(NSString *)content andPosition:(NSUInteger)position inContext:(NSManagedObjectContext *)context
{
	Pearl *pearl = [Pearl MR_createInContext:context];
	pearl.createdAt = [NSDate date];
	pearl.updatedAt = [NSDate date];
	pearl.content = content;
	pearl.position = @(position);
	return pearl;
}

+ (Pearl *)pearlWithContent:(NSString *)content andPosition:(NSUInteger)position
{
	return [self pearlWithContent:content andPosition:position inContext:[NSManagedObjectContext MR_contextForCurrentThread]];
}

+ (Pearl *)pearlWithPearl:(Pearl *)pearl inContext:(NSManagedObjectContext *)context
{
	Pearl *newPearl = [Pearl pearlWithContent:pearl.content
								  andPosition:[pearl.position integerValue]
									inContext:context];
	newPearl.createdAt = pearl.createdAt;
	newPearl.updatedAt = pearl.updatedAt;
	return newPearl;
}

+ (Pearl *)pearlWithPearl:(Pearl *)pearl
{
	return [self pearlWithPearl:pearl inContext:pearl.managedObjectContext];
}

- (BOOL)validateContent
{
	// TODO: trim
	return self.content.length > 0;
}


@end
