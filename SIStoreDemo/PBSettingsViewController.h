//
//  PBSettingsViewController.h
//  PearlBox
//
//  Created by Kevin Cao on 12-12-19.
//  Copyright (c) 2012年 sumi-sumi.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PBSettingsViewControllerDelegate;

@interface PBSettingsViewController : UITableViewController

@property (nonatomic, weak) id<PBSettingsViewControllerDelegate> delegate;

@end

@protocol PBSettingsViewControllerDelegate <NSObject>

- (void)settingsViewControllerDidChangeStore:(PBSettingsViewController *)viewController;

@end
