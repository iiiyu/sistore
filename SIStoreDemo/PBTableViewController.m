//
//  PBTableViewController.m
//  PearlBox
//
//  Created by Kevin Cao on 12-12-19.
//  Copyright (c) 2012年 sumi-sumi.com. All rights reserved.
//

#import "PBTableViewController.h"
//#import "Pearl+PBAddition.h"
#import "Pearl.h"
#import "PBEditViewController.h"
#import "PBSettingsViewController.h"
#import "SIStore.h"
#import "SVProgressHUD.h"

@interface PBTableViewController () <PBEditViewControllerDelegate, PBSettingsViewControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@end

@implementation PBTableViewController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	[self setupData];
	[self setupObservers];
	
//	[self seed];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];	
}

#pragma mark - Setup

- (void)setupData
{
	self.title = [SIStore isUsingLocalStore] ? @"Local" : ([SIStore iCloudStoreState] == SIICloudStoreStateNormal ? @"iCloud" : @"iCloud-Readonly");
	
	self.fetchedResultsController = [Pearl MR_fetchAllSortedBy:@"updatedAt"
													 ascending:NO
												 withPredicate:nil
													   groupBy:nil
													  delegate:self];
}

- (void)setupObservers
{
	NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
	[center addObserver:self
			   selector:@selector(iCloudDidBecomeAvailable:)
				   name:SIICloudDidBecomeAvailableNotification
				 object:[SIStore class]];
	[center addObserver:self
			   selector:@selector(iCloudDidBecomeUnvailable:)
				   name:SIICloudDidBecomeUnavailableNotification
				 object:[SIStore class]];
	
	// for debug
	[center addObserver:self
			   selector:@selector(contextDidChange:)
				   name:NSManagedObjectContextObjectsDidChangeNotification
				 object:[NSManagedObjectContext MR_defaultContext]];
	[center addObserver:self
			   selector:@selector(contextDidMergeChangesFromiCloud:)
				   name:kMagicalRecordDidMergeChangesFromiCloudNotification
				 object:[NSManagedObjectContext MR_defaultContext]];
}

- (void)seed
{
    [MagicalRecord saveUsingCurrentThreadContextWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        for (NSUInteger i = 0; i < 1000; i++) {
			[Pearl pearlWithContent:[NSString stringWithFormat:@"New Item %d", i]
						andPosition:0
						  inContext:localContext];
		}
    }];
    
//	[MagicalRecord saveInBackgroundWithBlock:^(NSManagedObjectContext *localContext) {
//		for (NSUInteger i = 0; i < 1000; i++) {
//			[Pearl pearlWithContent:[NSString stringWithFormat:@"New Item %d", i]
//						andPosition:0
//						  inContext:localContext];
//		}
//	}];
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier = @"ListCell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	Pearl *pearl = [self.fetchedResultsController objectAtIndexPath:indexPath];
	cell.textLabel.text = pearl.content;

	return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		[[self.fetchedResultsController objectAtIndexPath:indexPath] MR_deleteEntity];
//		[[NSManagedObjectContext MR_defaultContext] MR_save];
        [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];
	}
}

#pragma mark - Notifications

- (void)iCloudDidBecomeAvailable:(NSNotification *)notification
{
	[SVProgressHUD showWithStatus:NSLocalizedString(@"正在同步", nil)
						 maskType:SVProgressHUDMaskTypeClear];
	[SIStore setupICloudStoreCompletion:^{
		[self setupData];
		[self.tableView reloadData];
		[SVProgressHUD dismiss];
	}];
}

- (void)iCloudDidBecomeUnavailable:(NSNotification *)notification
{
	// go to readonly mode by doing setup again
	[SIStore setupICloudStoreCompletion:^{
		[self setupData];
		[self.tableView reloadData];
	}];
}

- (void)contextDidChange:(NSNotification *)notification
{
	NSLog(@"contextDidChange: %@", notification.userInfo);
}

- (void)contextDidMergeChangesFromiCloud:(NSNotification *)notification
{
	NSLog(@"contextDidMergeChangesFromiCloud: %@", notification.userInfo);
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ShowEdit"]) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
		Pearl *pearl = (Pearl *)[self.fetchedResultsController objectAtIndexPath:indexPath];
		PBEditViewController *viewController = segue.destinationViewController;
		viewController.pearl = pearl;
		viewController.context = [NSManagedObjectContext MR_defaultContext];
		viewController.delegate = self;
	} else if ([segue.identifier isEqualToString:@"ShowAdd"]) {
		NSManagedObjectContext *context = [NSManagedObjectContext MR_newMainQueueContext];
		context.parentContext = [NSManagedObjectContext MR_defaultContext];
		Pearl *pearl = [Pearl pearlWithContent:[NSString stringWithFormat:@"New Item %d", self.fetchedResultsController.fetchedObjects.count]
								   andPosition:0
									 inContext:context];
		
		PBEditViewController *viewController = segue.destinationViewController;
		viewController.context = context;
		viewController.pearl = pearl;
		viewController.delegate = self;
	} else if ([segue.identifier isEqualToString:@"ShowSettings"]) {
		PBSettingsViewController *viewController = segue.destinationViewController;
		viewController.delegate = self;
	}
}

#pragma mark - PBEditViewControllerDelegate

- (void)editViewController:(PBEditViewController *)viewController didFinishSave:(BOOL)save
{
	if (save && [viewController.pearl hasChanges]) {
//		[viewController.context MR_saveNestedContexts];
        [viewController.context MR_saveToPersistentStoreAndWait];
	}
	
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - PBSettingsViewControllerDelegate

- (void)settingsViewControllerDidChangeStore:(PBSettingsViewController *)viewController
{
	[self setupData];
	[self.tableView reloadData];
	
	[self.navigationController popViewControllerAnimated:YES];
}

@end
