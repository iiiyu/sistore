#import "_Pearl.h"

@interface Pearl : _Pearl {}
// Custom logic goes here.

+ (Pearl *)pearlWithContent:(NSString *)content andPosition:(NSUInteger)position inContext:(NSManagedObjectContext *)context;
+ (Pearl *)pearlWithContent:(NSString *)content andPosition:(NSUInteger)position;
+ (Pearl *)pearlWithPearl:(Pearl *)pearl inContext:(NSManagedObjectContext *)context;
+ (Pearl *)pearlWithPearl:(Pearl *)pearl;
- (BOOL)validateContent;

@end
