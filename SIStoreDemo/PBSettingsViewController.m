//
//  PBSettingsViewController.m
//  PearlBox
//
//  Created by Kevin Cao on 12-12-19.
//  Copyright (c) 2012年 sumi-sumi.com. All rights reserved.
//

#import "PBSettingsViewController.h"
#import <SVProgressHUD.h>
#import "SIDemoStore.h"
//#import "SIStore+PBMigrate.h"

@interface PBSettingsViewController ()

@property (weak, nonatomic) IBOutlet UITableViewCell *iCloudCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *localCell;

@end

@implementation PBSettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.iCloudCell.textLabel.enabled = NO;
	self.iCloudCell.userInteractionEnabled = NO;
	[SIDemoStore checkICloudAvailabilityCompletion:^(BOOL available) {
		if (available) {
			self.iCloudCell.textLabel.enabled = YES;
			self.iCloudCell.userInteractionEnabled = YES;
		}
	}];

	[self updateUI];
}

- (void)viewDidUnload
{
	[self setICloudCell:nil];
	[self setLocalCell:nil];
	[super viewDidUnload];
}

- (void)updateUI
{
	if ([SIDemoStore isUsingLocalStore]) {
		self.iCloudCell.accessoryType = UITableViewCellAccessoryNone;
		self.localCell.accessoryType = UITableViewCellAccessoryCheckmark;
	} else {
		self.iCloudCell.accessoryType = UITableViewCellAccessoryCheckmark;
		self.localCell.accessoryType = UITableViewCellAccessoryNone;
	}
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
		if ([SIDemoStore isUsingLocalStore]) {
			[self changeStoreLocation:NO];
		}
	} else {
		if (![SIDemoStore isUsingLocalStore]) {
			[self changeStoreLocation:YES];
		}
	}
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)changeStoreLocation:(BOOL)useLocalStore
{
	[SVProgressHUD showWithStatus:NSLocalizedString(@"正在切换", nil)
						 maskType:SVProgressHUDMaskTypeClear];
	
	[SIDemoStore migrateStore:useLocalStore
			   completion:^{
				   [SVProgressHUD dismiss];
				   [self updateUI];
				   [self.delegate settingsViewControllerDidChangeStore:self];
			   }];
	
	[self updateUI];
}


@end
