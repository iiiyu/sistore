// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Pearl.m instead.

#import "_Pearl.h"

const struct PearlAttributes PearlAttributes = {
	.archived = @"archived",
	.content = @"content",
	.createdAt = @"createdAt",
	.position = @"position",
	.test = @"test",
	.updatedAt = @"updatedAt",
};

const struct PearlRelationships PearlRelationships = {
};

const struct PearlFetchedProperties PearlFetchedProperties = {
};

@implementation PearlID
@end

@implementation _Pearl

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Pearl" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Pearl";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Pearl" inManagedObjectContext:moc_];
}

- (PearlID*)objectID {
	return (PearlID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"archivedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"archived"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"positionValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"position"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"testValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"test"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic archived;



- (BOOL)archivedValue {
	NSNumber *result = [self archived];
	return [result boolValue];
}

- (void)setArchivedValue:(BOOL)value_ {
	[self setArchived:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveArchivedValue {
	NSNumber *result = [self primitiveArchived];
	return [result boolValue];
}

- (void)setPrimitiveArchivedValue:(BOOL)value_ {
	[self setPrimitiveArchived:[NSNumber numberWithBool:value_]];
}





@dynamic content;






@dynamic createdAt;






@dynamic position;



- (int64_t)positionValue {
	NSNumber *result = [self position];
	return [result longLongValue];
}

- (void)setPositionValue:(int64_t)value_ {
	[self setPosition:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitivePositionValue {
	NSNumber *result = [self primitivePosition];
	return [result longLongValue];
}

- (void)setPrimitivePositionValue:(int64_t)value_ {
	[self setPrimitivePosition:[NSNumber numberWithLongLong:value_]];
}





@dynamic test;



- (BOOL)testValue {
	NSNumber *result = [self test];
	return [result boolValue];
}

- (void)setTestValue:(BOOL)value_ {
	[self setTest:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveTestValue {
	NSNumber *result = [self primitiveTest];
	return [result boolValue];
}

- (void)setPrimitiveTestValue:(BOOL)value_ {
	[self setPrimitiveTest:[NSNumber numberWithBool:value_]];
}





@dynamic updatedAt;











@end
