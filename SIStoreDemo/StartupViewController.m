//
//  StartupViewController.m
//  PearlBox
//
//  Created by Kevin Cao on 12-12-18.
//  Copyright (c) 2012年 sumi-sumi.com. All rights reserved.
//

#import "StartupViewController.h"
#import "SIStore.h"

NSString *PBUsedBeforeKey = @"PBUsedBeforeKey";

@interface StartupViewController () <UIAlertViewDelegate>

@end

@implementation StartupViewController

- (void)asyncNukeAndPave {
    
    NSFileCoordinator *fc = [[NSFileCoordinator alloc] initWithFilePresenter:nil];
    NSError *error = nil;
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *path = [[fm URLForUbiquityContainerIdentifier:nil] path];
    NSArray *subPaths = [fm subpathsAtPath:path];
    for (NSString *subPath in subPaths) {
        NSString *fullPath = [NSString stringWithFormat:@"%@/%@", path, subPath];
        [fc coordinateWritingItemAtURL:[NSURL fileURLWithPath:fullPath]
                               options:NSFileCoordinatorWritingForDeleting
                                 error:&error
                            byAccessor:^(NSURL *newURL) {
								NSError *blockError = nil;
								if ([fm removeItemAtURL:newURL error:&blockError]) {
									NSLog(@"Deleted file: %@", newURL);
								} else {
									NSLog(@"Error deleting file: %@\nError: %@", newURL, blockError);
								}
							}];
    }
	
    fc = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
//	[self asyncNukeAndPave];
//	abort();
	
	if (![[NSUserDefaults standardUserDefaults] boolForKey:PBUsedBeforeKey]) {
		[SIStore checkICloudAvailabilityCompletion:^(BOOL available) {
			if (available) {
				UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"同步选项", nil)
																	message:NSLocalizedString(@"是否启用iCloud同步？您可以随时在设置页中更改此项。", nil)
																   delegate:self
														  cancelButtonTitle:NSLocalizedString(@"iCloud存储", nil)
														  otherButtonTitles:@"本地存储", nil];
				[alertView show];
			} else {
				[SIStore setupLocalStoreCompletion:^{
					[self startup];
				}];
			}
		}];
	} else {
		[SIStore setupStoreUsingDefaultLocationCompletion:^{
			[self startup];
		}];
	}
}

- (void)startup
{
	[NSManagedObjectContext MR_defaultContext].stalenessInterval = 0;
	self.view.window.rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PBNavigationController"];
	
	[[NSUserDefaults standardUserDefaults] setBool:YES forKey:PBUsedBeforeKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0) {
		[SIStore setupICloudStoreCompletion:^{
			[self startup];
		}];
	} else {
		[SIStore setupLocalStoreCompletion:^{
			[self startup];
		}];
	}
}

@end
